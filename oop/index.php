<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");
echo "Name: $sheep->name <br>" ; // "shaun"
echo "Legs: $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Name: $kodok->name <br>";
echo "Legs: $kodok->legs <br>"; // 4
echo "Cold Blooded: $kodok->cold_blooded <br>"; // "no"
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
$sungokong->legs = 2;
echo "Name: $sungokong->name <br>" ;
echo "Legs: $sungokong->legs <br>"; //2
echo "Cold Blooded: $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"